package com.tripitapi.booking;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class BookingRequestDTO {
	private int bookingtype;
	private String fromlocation;
	private String tolocation;
	private String departdate;
	private int price;
	private String usertoken;
}
