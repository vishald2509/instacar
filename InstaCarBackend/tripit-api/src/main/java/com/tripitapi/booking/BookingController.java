package com.tripitapi.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tripitapi.account.UserSession;

@RestController
@RequestMapping("/booking")
public class BookingController {
	@Autowired
	private BookingService bookingServiceObject;
	
	//TODO book
	@PostMapping(path="/book")
	public String bookTrip(@RequestBody BookingRequestDTO bookingRequestObject) {
		return bookingServiceObject.bookTrip(bookingRequestObject);
	}
	
}
