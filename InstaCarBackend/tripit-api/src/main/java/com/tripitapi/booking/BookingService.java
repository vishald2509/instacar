package com.tripitapi.booking;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tripitapi.account.User;
import com.tripitapi.account.UserSession;
import com.tripitapi.account.UserSessionRepository;

@Service
public class BookingService {
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@Autowired
	private UserSessionRepository userSessionRepository;
	
	public String bookTrip(BookingRequestDTO bookingRequestObject)
	{
		String id= UUID.randomUUID().toString(); //Gennerate UUID
		UserSession userSession = userSessionRepository.findByToken(bookingRequestObject.getUsertoken());
		User user=userSession.getUser();
		Booking bookTrip= new Booking(id, user, bookingRequestObject.getBookingtype(), bookingRequestObject.getFromlocation(), bookingRequestObject.getTolocation(), bookingRequestObject.getDepartdate(), bookingRequestObject.getPrice());
		bookingRepository.save(bookTrip);
		return "Succesful Booked";
	}
	
}
