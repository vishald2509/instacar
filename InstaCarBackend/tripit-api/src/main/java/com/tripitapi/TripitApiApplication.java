package com.tripitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripitApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TripitApiApplication.class, args);
	}

}
