package com.tripitapi.account;

import com.tripitapi.utils.TripitapiConstants.LanguageType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class AddDriverRequestDTO {
	private String drivername;
	private int rate;
	private java.util.List<LanguageType> languages;
}
