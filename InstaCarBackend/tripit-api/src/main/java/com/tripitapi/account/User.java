package com.tripitapi.account;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection="user") @Data @AllArgsConstructor
public class User {
	@Id
	private String id;
	private String fullname;
	@Indexed(unique=true)
	private String email;	//Make Unique
	private String password;
	private long phno;		//Make Unique
	
	//TODO CREATED DATE MODIFIED DATE
	
}
