package com.tripitapi.account;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection = "usersession") @Data @AllArgsConstructor
public class UserSession {
	@Id
	private String id;
	@Indexed(unique=true)
	private String token;	//Make Unique
	private User user;		//add annotation onetoMany i.e to use @DBREF
}
