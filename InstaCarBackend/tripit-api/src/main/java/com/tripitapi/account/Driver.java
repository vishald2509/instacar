package com.tripitapi.account;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.tripitapi.utils.TripitapiConstants.LanguageType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection = "driver") @Data @AllArgsConstructor
public class Driver {
	@Id
	private String id;
	private String driverName;
	private int rate;
	private java.util.List<LanguageType> languages;
	
	//TODO CREATED DATE MODIFIED DATE
}
