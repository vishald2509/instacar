package com.tripitapi.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	private AccountService accountServiceObject;
	
	//TODO Signup
	@PostMapping(path="/signup")
	public String userSignup(@RequestBody SignupRequestDTO signupRequestObject) {
		return accountServiceObject.addUser(signupRequestObject);
	}
	//TODO SignIn
	@PostMapping(path="/signin")
	public String userSignin(@RequestBody SigninRequestDTO signinRequestObject) {
	
		return accountServiceObject.authenticateUser(signinRequestObject);
	}
	
	//TODO SignOut
	
	
	//TODO Add Driver
	@PostMapping(path="/adddriver")
	public String addDriver(@RequestBody AddDriverRequestDTO addDriverRequestObject) {
		return accountServiceObject.addDriver(addDriverRequestObject);
	}
	//TODO Delete Driver
}
