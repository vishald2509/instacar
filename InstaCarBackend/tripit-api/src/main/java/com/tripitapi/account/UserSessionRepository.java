package com.tripitapi.account;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserSessionRepository extends MongoRepository<UserSession, String>{
	void deleteByToken(String token);
	UserSession findByToken(String token);
}
