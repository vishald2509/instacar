package com.tripitapi.account;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SigninRequestDTO {
	private String userid;
	private String password;
}
