package io.projectmongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.projectmongodb.model.TokenAuthentication;

public interface TokenAuthenticationRepository extends MongoRepository<TokenAuthentication, String>{
	void deleteByToken(String token);
}
