package io.projectmongodb.dto;

public class ResponseDTO {

	public String message;

	public Object data;
	
	public ResponseDTO(Object data){
		this.data = data;
		this.message = "Success";
	}
}
