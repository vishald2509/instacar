package io.projectmongodb.exception;

public class ProjectmongodbException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8409444895133899471L;

	private String message;

	public ProjectmongodbException(String message) {
		super(message);
		this.message = message;
	}
}
