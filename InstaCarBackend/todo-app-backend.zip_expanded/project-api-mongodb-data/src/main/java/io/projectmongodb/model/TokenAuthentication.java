package io.projectmongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;


@Document(collection = "authentication") @AllArgsConstructor @Data
public class TokenAuthentication {

	@Id
	private String id;
	private String token;
	private int usertype;
	private User user;
	
}
