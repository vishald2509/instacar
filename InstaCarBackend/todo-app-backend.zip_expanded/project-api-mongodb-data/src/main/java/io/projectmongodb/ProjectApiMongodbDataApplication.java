package io.projectmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectApiMongodbDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectApiMongodbDataApplication.class, args);
	}

}
