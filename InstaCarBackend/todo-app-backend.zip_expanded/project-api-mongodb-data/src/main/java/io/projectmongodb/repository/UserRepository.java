package io.projectmongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.projectmongodb.model.User;

public interface UserRepository extends MongoRepository<User, String>{

	User findByUsername(String username);
}
