package io.projectmongodb.exception;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.projectmongodb.dto.ErrorMessage;
import io.projectmongodb.dto.ResponseDTO;

@RestControllerAdvice
public class AppExceptionHandler {

	private static Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

	@ExceptionHandler(ProjectmongodbException.class)
	public ResponseEntity<Object> ProjectmongodbExceptionHandler(ProjectmongodbException e) {
		String errorMessageDescription = e.getLocalizedMessage();
		if (errorMessageDescription == null) {
			errorMessageDescription = e.toString();
		}
		ErrorMessage errorMessage = new ErrorMessage(new Date(), errorMessageDescription);
		return new ResponseEntity<Object>(errorMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> internalServerException(RuntimeException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.ACCEPTED);
	}
}
